function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#showFile').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#inputFile").change(function() {
    readURL(this);
});

$("#btnSend").click(function() {
    $.post("", function(data) {
        console.log(data);
    });
});